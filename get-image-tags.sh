#!/usr/bin/env bash

# Get image tags available in Docker Hub for a given image.
# Supply image name as first, mandatory argument.
# Specify optionally a tag regex filter as second argument.

set -euo pipefail

[[ "${#}" -gt 0 ]] || {
  echo "Image name is expected as first, mandatory argument." >&2
  echo "Specify optionally a tag regex filter as second argument." >&2
  exit 1
}

TAG_REGEX_FILTER="${2:-}"

# shellcheck disable=SC2207
IMAGE_TAGS=($(wget -q "https://registry.hub.docker.com/v1/repositories/${1}/tags" -O - \
  | sed -e 's/[][]//g' -e 's/"//g' -e 's/ //g' \
  | tr '}' '\n' \
  | awk -F: '{print $3}' \
  | sort -r))

for image_tag in "${IMAGE_TAGS[@]}" ; do
  if [[ -z "${TAG_REGEX_FILTER}" ]] || [[ "${image_tag}" =~ ${TAG_REGEX_FILTER} ]]; then
    echo "${image_tag}"
  fi
done
