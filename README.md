<p align="center">
    <img src="https://gitlab.com/gitlab.magenolia/assets/-/raw/main/magenolia-logo.png" alt="Magenolia" width="250"/>
    <br>
    <img src="https://gitlab.com/gitlab.magenolia/assets/-/raw/main/magento-logo.png" alt="Magento" width="100"/>
    &nbsp;&nbsp;&nbsp;&nbsp;
    <img src="https://gitlab.com/gitlab.magenolia/assets/-/raw/main/mysql-logo.png" alt="MySQL" width="120"/>
</p>

[[_TOC_]]

# Magento MySQL server images

This repository contains MySQL server images with custom settings adapted for Magento 2.

The main purpose of these images is allowing to spin up a preconfigured MySQL server for Magento 2 almost effortlessly.

These images are built on top of [MySQL official ones](https://hub.docker.com/_/mysql).
This means that all documentation related with them also applies to these images, including environment variables and other settings.

## Supported platforms

- linux/amd64
- linux/arm64/v8

## Supported tags and respective `Dockerfile` and `magento.cnf` links

- `8.0` - [Dockerfile](https://gitlab.com/magenolia/docker-images/magento-mysql-server/-/raw/8.0/Dockerfile) - [magento.cnf](https://gitlab.com/magenolia/docker-images/magento-mysql-server/-/raw/8.0/context/conf.d/magento.cnf)
- `5.7` - [Dockerfile](https://gitlab.com/magenolia/docker-images/magento-mysql-server/-/raw/5.7/Dockerfile) - [magento.cnf](https://gitlab.com/magenolia/docker-images/magento-mysql-server/-/raw/5.7/context/conf.d/magento.cnf)
- `5.6` - [Dockerfile](https://gitlab.com/magenolia/docker-images/magento-mysql-server/-/raw/5.6/Dockerfile) - [magento.cnf](https://gitlab.com/magenolia/docker-images/magento-mysql-server/-/raw/5.6/context/conf.d/magento.cnf)

# Usage

There are several ways to use these images, depending on your environment setup strategy:

`1.` Docker approach:
- `a.` Using `docker run` to spin up containers manually.
- `b.` Using `docker-compose`.

`2.` Single / multiple MySQL servers:
- `a.` Using a single MySQL server for one or several Magento installations.
- `b.` Using a dedicated MySQL server container for each Magento installation.

Adapt all commands below as needed.

## Using `docker run`

### Using a single MySQL server for one or several Magento installations.

For commands below:
- Set `MYSQL_ROOT_PASSWORD` to a more secure password.
- Change `MYSQL_USER` and `MYSQL_PASSWORD` as appropriate.
- Host ports are customized to allow having several Mysql versions installed:
    - `8.0`: `3308`
    - `5.7`: `3307`
    - `5.6`: `3306`

#### 8.0

Create or update MySQL 8.0 server container:

```shell
# Pull latest image version.
docker pull "magenolia/magento-mysql-server:8.0"

# Recreate container from latest image version.
[ -z "$(docker ps -q -a -f name="magento-mysql-server-8.0")" ] || { \
  docker stop "magento-mysql-server-8.0" \
  && docker rm "magento-mysql-server-8.0" \
}

# Run container.
docker run -it -d \
  --name "magento-mysql-server-8.0" \
  --stop-timeout '60' \
  --restart 'unless-stopped' \
  -p '3308:3306' \
  -e 'MYSQL_ROOT_HOST=%' \
  -e "MYSQL_ROOT_PASSWORD=supersecretpassword" \
  -e 'MYSQL_USER=magento' \
  -e 'MYSQL_PASSWORD=magento' \
  -v "${HOME}/.mysql/data/8.0:/var/lib/mysql:rw,delegated" \
  'magenolia/magento-mysql-server:8.0'
```

Login into MySQL 8.0 server container:

```shell
docker exec -it -u mysql mysql-8.0 bash
```

#### 5.7

Create or update MySQL 5.7 server container:

```shell
# Pull latest image version.
docker pull "magenolia/magento-mysql-server:5.7"

# Recreate container from latest image version.
[ -z "$(docker ps -q -a -f name="magento-mysql-server-5.7")" ] || { \
  docker stop "magento-mysql-server-5.7" \
  && docker rm "magento-mysql-server-5.7" \
}

# Run container.
docker run -it -d \
  --name "magento-mysql-server-5.7" \
  --stop-timeout '60' \
  --restart 'unless-stopped' \
  -p '3307:3306' \
  -e 'MYSQL_ROOT_HOST=%' \
  -e "MYSQL_ROOT_PASSWORD=supersecretpassword" \
  -e 'MYSQL_USER=magento' \
  -e 'MYSQL_PASSWORD=magento' \
  -v "${HOME}/.mysql/data/5.7:/var/lib/mysql:rw,delegated" \
  'magenolia/magento-mysql-server:5.7'
```

Login into MySQL 5.7 server container:

```shell
docker exec -it -u mysql mysql-5.7 bash
```

#### 5.6

Create or update MySQL 5.6 server container:

```shell
# Pull latest image version.
docker pull "magenolia/magento-mysql-server:5.6"

# Recreate container from latest image version.
[ -z "$(docker ps -q -a -f name="magento-mysql-server-5.6")" ] || { \
  docker stop "magento-mysql-server-5.6" \
  && docker rm "magento-mysql-server-5.6" \
}

# Run container.
docker run -it -d \
  --name "magento-mysql-server-5.6" \
  --stop-timeout '60' \
  --restart 'unless-stopped' \
  -p '3306:3306' \
  -e 'MYSQL_ROOT_HOST=%' \
  -e "MYSQL_ROOT_PASSWORD=supersecretpassword" \
  -e 'MYSQL_USER=magento' \
  -e 'MYSQL_PASSWORD=magento' \
  -v "${HOME}/.mysql/data/5.6:/var/lib/mysql:rw,delegated" \
  'magenolia/magento-mysql-server:5.6'
```

Login into MySQL 5.6 server container:

```shell
docker exec -it -u mysql mysql-5.6 bash
```
