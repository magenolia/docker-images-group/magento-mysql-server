#!/usr/bin/env bash

# Pull image tags available in Docker Hub for a given image.
# Supply image name as first, mandatory argument.
# Specify optionally a tag regex filter as second argument.

set -euo pipefail

[[ "${#}" -gt 0 ]] || {
  echo "Image name is expected as first, mandatory argument." >&2
  echo "Specify optionally a tag regex filter as second argument." >&2
  exit 1
}

# Get current script directory.
CURRENT_DIR="$(dirname "$(realpath "${BASH_SOURCE[0]}")")"
export CURRENT_DIR

TAG_REGEX_FILTER="${2:-}"

mapfile -t IMAGE_TAGS < <( "${CURRENT_DIR}/get-image-tags.sh" "${1}" "${TAG_REGEX_FILTER}" )

for image_tag in "${IMAGE_TAGS[@]}" ; do
  if [[ -z "${TAG_REGEX_FILTER}" ]] || [[ "${image_tag}" =~ ${TAG_REGEX_FILTER} ]]; then
    docker pull -- "${1}:${image_tag}"
  fi
done

